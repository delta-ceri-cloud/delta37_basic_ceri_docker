# DELTA 37 Docker deployment
This installation is using docker on Linux/Ubuntu environment. Dockerization environment enables users for effectively set up DELTA for demo purposes. 
As the DELTA integration and automation is under continuous development, in case of facing issue please contact CERI support to troubleshoot or backup purposes.
Manual installation guide is also available in case of environment set up. 
This dockerization is primarily used for running automated setup for DELTA 3.7 version.

#### Detail Deployment manual 
Below instructions are for detail deployment manual: ```https://ceri-lahey.atlassian.net/l/c/y0Ar8UmR```

#### Detail User manual 
Below instructions are for detail user manual: ```https://ceri-lahey.atlassian.net/l/c/xytM3oSL```

#### Minimum Requirements 

* Ubuntu/Linux OS
* 4 CPUs 2.40 GHz
* 8 GB RAM 
* 100 MHz Ethernet network card
--------------------------------------------------------------------------------------------------------------------------------------------------------------

# Get Started
#### Install docker
Open command line terminal and run following commands

* `sudo apt-get update`
* `apt-get install docker`
* `apt-get install docker.io`

#### Install docker compose
Checkout docker-compose installation: ```https://ceri-lahey.atlassian.net/l/c/y0Ar8UmR``` or run following commands

* `sudo curl -L "https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)"  -o /usr/local/bin/docker-compose`
* `sudo mv /usr/local/bin/docker-compose /usr/bin/docker-compose`
* `sudo chmod +x /usr/bin/docker-compose`
* `docker-compose --version`

#### Install git
Repository include DELTA subrepository. It may prompt to enter bitbucket authorizationmultiple time.

* apt-get install git
* Clone repository:

	`git clone repository_link  --recurse-submodules`

Once repository cloned, Please checkout to "deltav3.7_2.0.1", new update available in DELTAlytics. 
To checkout repository :  

 *  `cd delta37_basic_ceri_docker`   
 *  `git checkout deltav3.7_2.0.1`  

Once you have specific branches from each, you can proceed with deployment

#### Running DELTA
Repository have default configs and credentials for setting up services. CERI highly recommend to change those 
configs by providing own credentials. Please see deployement manual for setting up configuration.If you want to install with default configuration, please skip this step.
Navigate to clone repository and run following commands
* `cd delta37_basic_ceri_docker`  
* `docker-compose up --build -d`

Docker build approximately taking 5-10 min depend on database import.
Once build complete, run the follwing command to see all services running inside containers:  
`docker ps`

Upon successfull completion, make sure to check logs as specified in above documentation. You can check logs for each services using following commands:   

* `docker logs deltadb`  
* `docker logs dam`  
* `docker logs deltaapp`  

Once completes, you should able to visit DELTA application and login using testuser credentials on following link:  
```http://hostip:8080/Delta3/index.html```

#### Setting up DELTALytics configuration

Once testuser able to login, final step is to set up DELTALytics config. Again navigate to linux instance for setting up DELTALytics config.
Repository have default credential for DELTALytics located in repository : delta37_basic_ceri_docker/deltalytics/credentials.yaml
However CERI highly recommend to set up your own credentials. Please see deployment manual mentioned above.

To apply this default/custom credentials inside running docker container execute below command :

* `docker exec -it dam /bin/bash`
* `apt-get update`
* `apt-get install vim`

You should able to see credential.yaml file located inside container /deltalytics/credentials.yaml
To open and edit file enter below command

* `vi credentials.yaml`

Press 'i' to edit credential file inside docker container and enter/copy details which are provided in file located in : delta37_basic_ceri_docker/deltalytics/credentials.yaml
Once complete, press `:wq` to save this file and type `exit` to exits from docker container.

enter following command to restart docker container so that new configuration will be applied:

* `docker restart dam`

Once again verify docker status "UP" and running by using `docker ps` for all containers. 

Upon successful configuration you should able to navigate to DELTA application and testuser should able to run default test studies available in test project.
Visit DELTA application and login using testuser credentials on following link:  
```http://hostip:8080/Delta3/index.html```


Upon successful configuration you should able to navigate to DELTA application and testuser should able to run default test studies available in test project.
This step will complete DELTA installation and configuration.

Make sure your firewall, symentec endpoint or any other protection is not DELTA blocking application.
Please ensure firewall, Ports like 22,443, 3306 (MySQl) and 1433(MSSQL) for host machine and database server.

-----------------------------------------------------------------------------------------------------------------------------------------------