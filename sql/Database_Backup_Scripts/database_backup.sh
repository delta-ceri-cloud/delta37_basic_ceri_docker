#!/bin/bash

echo Database back up initiated...
echo "Database configuration Script executed from: ${PWD}"

BASEDIR=$(dirname $0)
echo "Script location: ${BASEDIR}"


. ${BASEDIR}/conf/backupCnf.config


if [ -d "/home/Backup" ]
then
    echo "Directory /path/to/dir exists."
    cd /home/Backup
    dt=$(date +"%Y-%m-%d-%H:%M:%S");
    echo "$dt"
    mkdir /home/Backup/"Backup_""$dt"
    cd /home/Backup/"Backup_""$dt"
    echo Backup directory created with timestamp.
    docker exec deltadb /usr/bin/mysqldump -u"$user"  --password="$password"  delta3 > /home/Backup/"Backup_""$dt"/delta3.sql
    docker exec deltadb /usr/bin/mysqldump -u"$user"  --password="$password"  delta3_data > /home/Backup/"Backup_""$dt"/delta3_data.sql
    docker exec deltadb /usr/bin/mysqldump -u"$user"  --password="$password"  delta3_analytics > /home/Backup/"Backup_""$dt"/delta3_analytics.sql
    docker exec deltadb /usr/bin/mysqldump -u"$user"  --password="$password"  d3_test_datasets > /home/Backup/"Backup_""$dt"/d3_test_datasets.sql
    docker exec deltadb /usr/bin/mysqldump -u"$user"  --password="$password"  deltalytics  > /home/Backup/"Backup_""$dt"/deltalytics.sql
    echo Back up complete in subdirectory

else
    echo "Directory /path/to/dir does not exists. Creating Backup directory"
    mkdir /home/Backup
    mkdir /home/Backup/"Backup_""$(date +"%Y-%m-%d")"
    cd /home/Backup/"Backup_""$(date +"%Y-%m-%d")"
    echo Backup directory created...
    docker exec deltadb /usr/bin/mysqldump -u"$user"  --password="$password"  delta3 > /home/Backup/"Backup_""$(date +"%Y-%m-%d")"/delta3.sql
    docker exec deltadb /usr/bin/mysqldump -u"$user"  --password="$password"  delta3_data > /home/Backup/"Backup_""$(date +"%Y-%m-%d")"/delta3_data.sql
    docker exec deltadb /usr/bin/mysqldump -u"$user"  --password="$password"  delta3_analytics > /home/Backup/"Backup_""$(date +"%Y-%m-%d")"/delta3_analytics.sql
    docker exec deltadb /usr/bin/mysqldump -u"$user"  --password="$password"  d3_test_datasets > /home/Backup/"Backup_""$(date +"%Y-%m-%d")"/d3_test_datasets.sql
    docker exec deltadb /usr/bin/mysqldump -u"$user"  --password="$password"  deltalytics  > /home/Backup/"Backup_""$(date +"%Y-%m-%d")"/deltalytics.sql

echo Back up process complete !!!
fi