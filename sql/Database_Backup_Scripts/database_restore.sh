#!/bin/bash

echo Restoring database ... 

echo Database files path: $1

cat $1/delta3.sql | docker exec -i deltadb /usr/bin/mysql -u $2 --password=$3 delta3

cat $1/delta3_data.sql | docker exec -i deltadb /usr/bin/mysql -u $2 --password=$3 delta3_data 

cat $1/delta3_analytics.sql | docker exec -i deltadb /usr/bin/mysql -u $2 --password=$3 delta3_analytics

cat $1/d3_test_datasets.sql | docker exec -i deltadb /usr/bin/mysql -u $2 --password=$3 d3_test_datasets

echo Restore Complete!!!
