-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: delta3
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d3_desc_stats`
--

DROP TABLE IF EXISTS `d3_desc_stats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `d3_desc_stats` (
  `idStats` int(11) NOT NULL,
  `idOrganization` int(11) NOT NULL,
  `idModel` int(11) NOT NULL,
  `name` varchar(256) DEFAULT NULL,
  `stats` mediumtext,
  `status` varchar(50) DEFAULT NULL,
  `flat` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idStats`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d3_desc_stats`
--

LOCK TABLES `d3_desc_stats` WRITE;
/*!40000 ALTER TABLE `d3_desc_stats` DISABLE KEYS */;
INSERT INTO `d3_desc_stats` VALUES (1,1,1,NULL,'{\"type\":\"ALL\",\"statistics\": [{\"filter\":\"\",\"field\":\"married\",\"source\":\"Source\",\"min\":\"0\",\"mean\":\"0\",\"max\":\"0\",\"value25\":\"0\",\"value75\":\"0\",\"median\":\"0\",\"std\":\"0\",\"count\":1,\"nullCount\":0,\"zeroCount\":1,\"notNullCount\":1,\"notZeroCount\":0,\"nullPercent\":0,\"zeroPercent\":100,\"notNullPercent\":100,\"notZeroPercent\":0,\"class\":\"Risk Factor\",\"kind\":\"Continuous\",\"formula\":\"\",\"description\":\"\"},{\"filter\":\"\",\"field\":\"caseid\",\"source\":\"Source\",\"min\":\"1\",\"mean\":\"1\",\"max\":\"1\",\"value25\":\"1\",\"value75\":\"1\",\"median\":\"1\",\"std\":\"0\",\"count\":1,\"nullCount\":0,\"zeroCount\":0,\"notNullCount\":1,\"notZeroCount\":1,\"nullPercent\":0,\"zeroPercent\":0,\"notNullPercent\":100,\"notZeroPercent\":100,\"class\":\"Case ID\",\"kind\":\"Continuous\",\"formula\":\"\",\"description\":\"\"},{\"filter\":\"\",\"field\":\"ctrdate\",\"source\":\"Source\",\"min\":0,\"mean\":0,\"max\":0,\"value25\":0,\"value75\":0,\"median\":0,\"std\":0,\"count\":1,\"nullCount\":0,\"nullPercent\":0,\"notNullCount\":1,\"notNullPercent\":100,\"class\":\"Sequencer\",\"kind\":\"Date\",\"formula\":\"\",\"description\":\"\"}]}',NULL,0,1,1,'2020-12-19 05:15:25',1,'2020-12-19 05:16:13'),(2,1,1,NULL,'{\"type\":\"ALL\",\"statistics\": [{\"filter\":\"\",\"field\":\"married\",\"source\":\"Source\",\"min\":\"0\",\"mean\":\"0\",\"max\":\"0\",\"value25\":\"0\",\"value75\":\"0\",\"median\":\"0\",\"std\":\"0\",\"count\":1,\"nullCount\":0,\"zeroCount\":1,\"notNullCount\":1,\"notZeroCount\":0,\"nullPercent\":0,\"zeroPercent\":100,\"notNullPercent\":100,\"notZeroPercent\":0,\"class\":\"Risk Factor\",\"kind\":\"Continuous\",\"formula\":\"\",\"description\":\"\"},{\"filter\":\"\",\"field\":\"caseid\",\"source\":\"Source\",\"min\":\"1\",\"mean\":\"1\",\"max\":\"1\",\"value25\":\"1\",\"value75\":\"1\",\"median\":\"1\",\"std\":\"0\",\"count\":1,\"nullCount\":0,\"zeroCount\":0,\"notNullCount\":1,\"notZeroCount\":1,\"nullPercent\":0,\"zeroPercent\":0,\"notNullPercent\":100,\"notZeroPercent\":100,\"class\":\"Case ID\",\"kind\":\"Continuous\",\"formula\":\"\",\"description\":\"\"},{\"filter\":\"\",\"field\":\"ctrdate\",\"source\":\"Source\",\"min\":0,\"mean\":0,\"max\":0,\"value25\":0,\"value75\":0,\"median\":0,\"std\":0,\"count\":1,\"nullCount\":0,\"nullPercent\":0,\"notNullCount\":1,\"notNullPercent\":100,\"class\":\"Sequencer\",\"kind\":\"Date\",\"formula\":\"\",\"description\":\"\"}]}',NULL,1,1,1,'2020-12-19 05:16:13',1,'2020-12-19 05:16:13');
/*!40000 ALTER TABLE `d3_desc_stats` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-20 16:33:49
