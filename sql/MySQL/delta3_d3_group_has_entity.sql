-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: delta3
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d3_group_has_entity`
--

DROP TABLE IF EXISTS `d3_group_has_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `d3_group_has_entity` (
  `Group_idGroup` int(11) NOT NULL,
  `Entity_idEntity` int(11) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Group_idGroup`,`Entity_idEntity`),
  KEY `fk_Group_has_Entity_idx` (`Group_idGroup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d3_group_has_entity`
--

LOCK TABLES `d3_group_has_entity` WRITE;
/*!40000 ALTER TABLE `d3_group_has_entity` DISABLE KEYS */;
INSERT INTO `d3_group_has_entity` VALUES (0,1,'studies',1,'2020-12-19 05:27:38',1,'2020-12-19 05:27:38',1),(0,2,'studies',1,'2020-12-19 05:28:13',1,'2020-12-19 05:28:13',1),(0,3,'studies',1,'2020-12-19 05:28:41',1,'2020-12-19 05:28:41',1),(0,4,'studies',1,'2020-12-19 05:29:22',1,'2020-12-19 05:29:22',1),(0,5,'studies',1,'2020-12-19 05:29:47',1,'2020-12-19 05:29:47',1),(0,6,'studies',1,'2020-12-19 05:30:11',1,'2020-12-19 05:30:11',1),(0,7,'studies',1,'2020-12-19 05:30:32',1,'2020-12-19 05:30:32',1),(0,8,'studies',1,'2020-12-19 05:31:08',1,'2020-12-19 05:31:08',1),(0,9,'studies',1,'2020-12-19 05:31:36',1,'2020-12-19 05:31:36',1),(0,10,'studies',1,'2020-12-19 05:32:29',1,'2020-12-19 05:32:29',1),(0,11,'studies',1,'2020-12-19 05:32:57',1,'2020-12-19 05:32:57',1),(0,12,'studies',1,'2020-12-19 05:33:27',1,'2020-12-19 05:33:27',1),(0,13,'studies',1,'2020-12-19 05:33:52',1,'2020-12-19 05:33:52',1);
/*!40000 ALTER TABLE `d3_group_has_entity` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-20 16:33:42
