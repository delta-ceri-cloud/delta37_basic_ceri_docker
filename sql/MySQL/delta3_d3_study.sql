-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: delta3
-- ------------------------------------------------------
-- Server version	5.7.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `d3_study`
--

DROP TABLE IF EXISTS `d3_study`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `d3_study` (
  `idStudy` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `status` varchar(1024) DEFAULT NULL,
  `idOrganization` int(11) NOT NULL,
  `idGroup` int(11) DEFAULT NULL,
  `idModel` int(11) NOT NULL,
  `ModelColumn_idOutcome` int(11) DEFAULT NULL,
  `ModelColumn_idTreatment` int(11) DEFAULT NULL,
  `ModelColumn_idKey` int(11) DEFAULT NULL,
  `ModelColumn_idDate` int(11) DEFAULT NULL,
  `Stat_idStat` int(11) DEFAULT NULL,
  `Method_idMethod` int(11) DEFAULT NULL,
  `Filter_idFilter` int(11) DEFAULT NULL,
  `methodParams` varchar(10000) DEFAULT NULL,
  `isMissingDataUsed` tinyint(1) DEFAULT NULL,
  `isGenericId` tinyint(1) DEFAULT NULL,
  `isChunking` tinyint(1) DEFAULT NULL,
  `chunkingBy` varchar(45) DEFAULT NULL,
  `dumpData` tinyint(1) DEFAULT '0',
  `sendAllColumns` tinyint(1) DEFAULT '0',
  `outcomePositive` tinyint(1) DEFAULT '0',
  `autoExecute` tinyint(1) NOT NULL DEFAULT '1',
  `startTS` timestamp NULL DEFAULT NULL,
  `endTS` timestamp NULL DEFAULT NULL,
  `Alert_idAlert` int(11) DEFAULT NULL,
  `statPackageVer` varchar(45) DEFAULT NULL,
  `originalStudyId` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `confidenceInterval` double DEFAULT NULL,
  `confidenceInterval2` double DEFAULT NULL,
  `overwriteResults` tinyint(1) DEFAULT NULL,
  `localStatPackage` tinyint(1) DEFAULT NULL,
  `remoteStatPackage` tinyint(1) DEFAULT NULL,
  `LastProcess_idProcess` int(11) DEFAULT NULL,
  `submittedTS` varchar(45) DEFAULT NULL,
  `processedTS` timestamp NULL DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdTS` timestamp NULL DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedTS` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idStudy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `d3_study`
--

LOCK TABLES `d3_study` WRITE;
/*!40000 ALTER TABLE `d3_study` DISABLE KEYS */;
INSERT INTO `d3_study` VALUES (1,'LR Create','LR Create Sample','New',1,0,1,0,0,0,0,1,2,0,'',1,0,0,NULL,0,0,0,1,'2000-01-01 05:00:00','2020-12-31 05:00:00',0,'17',0,1,95,NULL,0,1,0,NULL,NULL,NULL,1,'2020-12-19 05:00:00',1,'2020-12-19 05:36:00'),(2,'LR Apply','LR Apply Sample','New',1,0,1,0,0,0,0,1,3,0,'',1,0,0,NULL,0,0,0,1,'2000-01-01 05:00:00','2020-12-31 05:00:00',0,'17',0,1,95,NULL,0,1,0,NULL,NULL,NULL,1,'2020-12-19 05:00:00',1,'2020-12-19 05:36:24'),(3,'LR Analysis','LR Analysis Sample','New',1,0,1,0,0,0,0,1,1,0,'',1,0,0,NULL,0,0,0,1,'2020-01-02 05:00:00','2020-12-31 05:00:00',0,'17',0,1,95,NULL,0,1,0,NULL,NULL,NULL,1,'2020-12-19 05:00:00',1,'2020-12-19 05:37:09'),(4,'PA Create','PA Create Sample','New',1,0,1,0,0,0,0,1,6,0,'',1,0,0,NULL,0,0,0,1,'2020-01-01 05:00:00','2020-12-31 05:00:00',0,'17',0,1,95,NULL,0,1,0,NULL,NULL,NULL,1,'2020-12-19 05:00:00',1,'2020-12-19 05:38:03'),(5,'PA Match','PA Match Sample','New',1,0,1,0,0,0,0,1,7,0,'',1,0,0,NULL,0,0,0,1,'2020-01-01 05:00:00','2020-12-31 05:00:00',0,'17',0,1,95,NULL,0,1,0,NULL,NULL,NULL,1,'2020-12-19 05:00:00',1,'2020-12-19 05:38:45'),(6,'PA Outcomes','PA Outcomes Sample','New',1,0,1,0,0,0,0,1,8,0,'',1,0,0,NULL,0,0,0,1,'2000-01-01 05:00:00','2020-12-31 05:00:00',0,'17',0,1,95,NULL,0,1,0,NULL,NULL,NULL,1,'2020-12-19 05:00:00',1,'2020-12-19 05:39:25'),(7,'Propensity','Propensity Sample','New',1,0,1,0,0,0,0,1,5,0,'',1,0,0,NULL,0,0,0,1,'2000-01-01 05:00:00','2020-12-31 05:00:00',0,'17',0,1,95,NULL,0,1,0,NULL,NULL,NULL,1,'2020-12-19 05:00:00',1,'2020-12-19 05:41:34'),(8,'Unmatched Survival','Unmatched Survival Sample','New',1,0,1,0,0,0,0,1,9,0,'',1,0,0,NULL,0,0,0,1,'2020-01-01 05:00:00','2020-12-31 05:00:00',0,'17',0,1,95,NULL,0,1,0,NULL,NULL,NULL,1,'2020-12-19 05:00:00',1,'2020-12-19 05:41:40'),(9,'Matched survical','Matched survical Sample','New',1,0,1,0,0,0,0,1,10,0,'',1,0,0,NULL,0,0,0,1,'2020-01-01 05:00:00','2020-12-31 05:00:00',0,'17',0,1,95,NULL,0,1,0,NULL,NULL,NULL,1,'2020-12-19 05:00:00',1,'2020-12-19 05:41:44'),(10,'RA SPRT','RA SPRT Sample','New',1,0,1,0,0,0,0,1,11,0,'',1,0,0,NULL,0,0,0,1,'2020-01-01 05:00:00','2020-12-31 05:00:00',0,'17',0,1,95,NULL,0,1,0,NULL,NULL,NULL,1,'2020-12-19 05:00:00',1,'2020-12-19 05:41:48'),(11,'IPTW','IPTW Sample','New',1,0,1,0,0,0,0,1,12,0,'',1,0,0,NULL,0,0,0,1,'2020-01-01 05:00:00','2020-12-31 05:00:00',0,'17',0,1,95,NULL,0,1,0,NULL,NULL,NULL,1,'2020-12-19 05:00:00',1,'2020-12-19 05:41:53'),(12,'PS Stratification','Propensity Stratification Sample','New',1,0,1,0,0,0,0,1,13,0,'',1,0,0,NULL,0,0,0,1,'2020-01-01 05:00:00','2020-12-31 05:00:00',0,'17',0,1,95,NULL,0,1,0,NULL,NULL,NULL,1,'2020-12-19 05:00:00',1,'2020-12-19 05:41:57'),(13,'PS WBO','PS WBO Sample','New',1,0,1,0,0,0,0,1,14,0,'',1,0,0,NULL,0,0,0,1,'2020-01-01 05:00:00','2020-12-31 05:00:00',0,'17',0,1,95,NULL,0,1,0,NULL,NULL,NULL,1,'2020-12-19 05:00:00',1,'2020-12-19 05:42:01');
/*!40000 ALTER TABLE `d3_study` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-20 16:33:41
