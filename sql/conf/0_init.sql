set global sql_mode='';

CREATE DATABASE delta3;

CREATE DATABASE delta3_data;

CREATE DATABASE delta3_analytics;

CREATE DATABASE d3_test_datasets;

CREATE DATABASE deltalytics;

CREATE USER 'defaultusername'@'%' IDENTIFIED BY 'defaultpassword';
GRANT ALL PRIVILEGES ON *.* TO 'defaultusername'@'%' IDENTIFIED BY 'defaultpassword';
FLUSH PRIVILEGES;

CREATE USER 'defaultusername'@'localhost' IDENTIFIED BY 'defaultpassword';
GRANT ALL PRIVILEGES ON *.* TO 'defaultusername'@'localhost' IDENTIFIED BY 'defaultpassword';
FLUSH PRIVILEGES;

CREATE USER 'd3TestUser'@'%' IDENTIFIED BY '123';
GRANT SELECT, INSERT, UPDATE, DELETE, ALTER, DROP ON d3_test_datasets.* TO 'd3TestUser'@'%'IDENTIFIED BY '123';
FLUSH PRIVILEGES;

CREATE USER 'd3TestUser'@'localhost' IDENTIFIED BY '123';
GRANT SELECT, INSERT, UPDATE, DELETE, ALTER, DROP ON d3_test_datasets.* TO 'd3TestUser'@'localhost'IDENTIFIED BY '123';
FLUSH PRIVILEGES;
